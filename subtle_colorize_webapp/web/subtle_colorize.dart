//   Copyright 2013 François CABOUAT
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import 'dart:html';
import 'dart:math';
import 'package:bot/bot.dart';

// ---------------------------------------------------------------------------
// Globals
// ---------------------------------------------------------------------------

// Checks if an image has been loaded
bool loaded = false;

// RGB helpers
const int R = 0, G = 1, B = 2;
const List<int> colors = const [R, G, B];

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------

void main() {

  // When a file is loaded in the file input, load image in the canvas
  query("#imageLoader").onChange.listen(handleFile);

  // When the whole "form" is submitted
  query("#submit").onClick.listen(handleSubmit);

}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Load image in the canvas
// ---------------------------------------------------------------------------

// Handle the file
void handleFile(Event inputEvent) {
  FileUploadInputElement input_files = query("#imageLoader");
  FileList files = input_files.files;

  if (files.length > 0) {
    File file = files.item(0);

    // Read the loaded file
    FileReader reader = new FileReader();
    // When the image data is loaded in memory...
    reader.onLoad.listen(handleImage);
    reader.readAsDataUrl(file);
  }
}

// Draw a memory-loaded image on the canvas
void handleImage(Event fileEvent) {

  // Load a new image on the canvas
  ImageElement image = new ImageElement();
  image.src = fileEvent.target.result;

  image.onLoad.listen((event) {
    // Get canvas
    CanvasElement canvas = query("#imageCanvas");
    CanvasRenderingContext2D ctxt = canvas.getContext("2d");

    // Draw on canvas
    canvas.width = image.width;
    canvas.height = image.height;
    ctxt.drawImage(image, 0, 0);
    loaded = true;
  });

}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Process the "form" submitted event & transforms the image in the canvas
// ---------------------------------------------------------------------------

// React to the submit event
void handleSubmit(Event actionEvent) {

  // Submit only when an image is loaded
  if (loaded) {

    // Get the "subtle" checkbox
    bool subtle = !query("#subtle").checked;

    // Get the color hex-value
    String hexColor = query("#target_color").value;

    // Try converting hex -> rgb (bot lib)
    try {
      RgbColor target_color = new RgbColor.fromHex(hexColor);
      // Then process the image
      process(target_color, subtle);

    } on DetailedArgumentError catch (e) {
      window.alert("Please give a valid hex-color (#AABBCC format).");
    }
  }
}

// Process the image
void process(RgbColor target_color, bool subtle) {

  // Get canvas pixels (ImageData)
  CanvasElement canvas = query("#imageCanvas");
  CanvasRenderingContext2D ctxt = query("#imageCanvas").getContext("2d");
  ImageData data  = ctxt.getImageData(0, 0, canvas.width, canvas.height);

  // Compute Image's average color
  List<int> average = averageColor(data.data, data.width * data.height);

  // Get target_color as List<int>
  List<int> target = [target_color.r, target_color.g, target_color.b];

  // Get the new transformed ImageData
  ImageData result = subtle ?
      getSubtle(data, average, target) : getNotSubtle(data, average, target);

  // Upload the transformed ImageData to the canvas
  ctxt.putImageData(data, 0, 0);

  // Save the canvas image
  window.open(canvas.toDataUrl('image/png'), "image.png");
}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Image calculations
// ---------------------------------------------------------------------------

// Returns the average color from an ImageData list
List<int> averageColor(List<int> data_list, int pixelsCount) {

  List<int> count = [0, 0, 0];
  List<int> avg = [0, 0, 0];

  // For each color channel of each pixel, add it to count
  var add_color_value = (int color, int color_index) {
    count[color] += data_list[color_index];
  };

  foreach_channel(data_list, add_color_value);

  // Get the average
  colors.forEach((color) {
    avg[color] = toPx(count[color]~/pixelsCount);
  });

  return avg;
}

// Transforms an ImageData (color translation)
ImageData getSubtle(ImageData data, List<int> avg_color, List<int> target) {

  // Get the actual list
  List<int> data_list = data.data;

  // For each color channel of each pixel, translate it
  var translate_color_value = (int color, int color_index) {
    data_list[color_index] = toPx(
      data_list[color_index] + target[color] - avg_color[color]
    );
  };

  foreach_channel(data_list, translate_color_value);

  return data;
}

// Transforms an ImageData (color multiplication)
ImageData getNotSubtle(ImageData data, List<int> avg_color, List<int> target) {

  // Get the actual list
  List<int> data_list = data.data;

  // Compute factors
  List<num> colorFactors = [0, 0, 0];

  colors.forEach((color) {
    colorFactors[color] = target[color] / avg_color[color];
  });

  // For each color channel of each pixel, multiply it
  var scale_color_value = (int color, int color_index) {
    data_list[color_index] = toPx(
      (data_list[color_index] * colorFactors[color]).toInt()
    );
  };

  foreach_channel(data_list, scale_color_value);

  return data;
}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Utility functions
// ---------------------------------------------------------------------------

// Apply a function to the R, G, and B color channels (ignores alpha channel)
void foreach_channel(List<int> data_list, Function func) {

  // Iterate the image_data list (4 by 4)
  for (int i = 0; i < data_list.length; i += 4) {
    func(R, i + R);
    func(G, i + G);
    func(B, i + B);
  }
}

// Force value into a pixel color value range (0, 255)
int toPx(int n) {
  return min(255, max(0, n));
}

// ---------------------------------------------------------------------------