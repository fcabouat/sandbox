#!/usr/bin/python
# -*- coding: utf-8 -*-

#   Copyright 2013 François CABOUAT

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#       http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import re, os, struct, argparse
import Image

def main():
    # -------------------------------------------------------------------------
    # Command line parsing
    
    parser = argparse.ArgumentParser(description='This program shifts the average color of an image (creates a new image next to the original).')
    parser.add_argument('input_file', metavar='input-file', type=str, help='Input file name')
    parser.add_argument('color', metavar='color', type=str, help='Destination color, "(R,G,B)" or "#aabbcc" format.')
    parser.add_argument('-s', '--sharp', dest='sharp', action='store_true', help='Sharp result algorithm')
    
    args = parser.parse_args()
    
    # -------------------------------------------------------------------------
    
    # -------------------------------------------------------------------------
    # Parse color (hex or rgb) to an rgb tuple
    
    rgbcode = None
    
    if args.color[0] == '#':
        
        hexcode = args.color[1:]
        
        if len(hexcode) == 3:
            hexcode = hexcode[0] + hexcode[0] + hexcode[1] + hexcode[1] + hexcode[2] + hexcode[2]
            
        if len(hexcode) != 6:
            raise Exception("Error: invalid hexcode")
        
        rgbcode = struct.unpack('BBB', hexcode.decode('hex'))
    
    else:
        rgbcode = tuple(int(v) for v in re.findall("[0-9]+", args.color))
        
    if len(rgbcode) != 3:
        raise Exception("Error: number of color bands")
    
    destR, destG, destB = rgbcode
    
    if destR not in range(0, 256):
        raise Exception("Error: Red band not in range")
    
    if destG not in range(0, 256):
        raise Exception("Error: Green band not in range")
    
    if destB not in range(0, 256):
        raise Exception("Error: Blue band not in range")
    
    # -------------------------------------------------------------------------
    
    # -------------------------------------------------------------------------
    # Compute input image's average color per color band
    
    im = Image.open(args.input_file).convert("RGB")
    
    colorList = im.getcolors()
    
    count, countR, countG, countB = (0, 0, 0, 0)
    
    for (colorCount, (inputR, inputG, inputB)) in colorList:
        count += colorCount
        countR += colorCount * inputR
        countG += colorCount * inputG
        countB += colorCount * inputB
    
    # Make sure to use float division (if still running python 2)
    averageR = float(countR) / count
    averageG = float(countG) / count
    averageB = float(countB) / count
    
    print("Average R: ", averageR, "Average G: ", averageG, "Average B: ", averageB, "\n")
    
    if averageR == 0:
        raise Exception("Error: Red band's average not in range... can't divide by 0")
    
    if averageG == 0:
        raise Exception("Error: Green band's average not in range... can't divide by 0") 
    
    if averageB == 0:
        raise Exception("Error: Blue band's average not in range... can't divide by 0")
    
    # -------------------------------------------------------------------------
    
    # -------------------------------------------------------------------------
    # Shift pixels to their new color
    
    source = im.split()

    R, G, B = 0, 1, 2
    
    if args.sharp:
        # Colors are multiplied
        rFactor = float(destR) / averageR
        gFactor = float(destG) / averageG
        bFactor = float(destB) / averageB
        
        source[R].paste(source[R].point(lambda i: min(255, max(0, i * rFactor))))
        source[G].paste(source[G].point(lambda i: min(255, max(0, i * gFactor))))
        source[B].paste(source[B].point(lambda i: min(255, max(0, i * bFactor))))
        
    else:
        # Colors are translated
        source[R].paste(source[R].point(lambda i: min(255, max(0, destR + (i - averageR)))))
        source[G].paste(source[G].point(lambda i: min(255, max(0, destG + (i - averageG)))))
        source[B].paste(source[B].point(lambda i: min(255, max(0, destB + (i - averageB)))))
    
    im = Image.merge(im.mode, source)
    
    # -------------------------------------------------------------------------
    
    # -------------------------------------------------------------------------
    # Finally : save output file
    
    output_file, e = os.path.splitext(args.input_file)
    output_file += ".new.png"
    
    try:
        im.save(output_file)
    except IOError:
        print("File saving/conversion error: ", output_file)
        
    # -------------------------------------------------------------------------
    
if __name__ == "__main__":
    main()